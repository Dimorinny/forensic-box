#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include "AbstractWidget.h"
#include <QWidget>

namespace Ui {
class MainWidget;
}

class MainWidget : public AbstractWidget
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();

private:
    Ui::MainWidget *ui;
};

#endif // MAINWIDGET_H
