#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "AbstractWidget.h"
#include "MainWidget.h"
#include <QGridLayout>
#include <QMainWindow>
#include <WidgetIdContainer.h>
#include <QGridLayout>
#include <QDebug>
#include <QHash>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    AbstractWidget* getWidgetById(int idWidget);

private:
    QHash<int, AbstractWidget*> widgetsHash;
    AbstractWidget* currentWidget;

    void setMainWidget(int idWidget);

public slots:
    void setMainWidgetSlot(int idWidget, bool deleteFlag);
};

#endif // MAINWINDOW_H
