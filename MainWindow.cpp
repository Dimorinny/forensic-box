#include "MainWindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), currentWidget(nullptr) {

    setMainWidget(WidgetIdContainer::MAIN_WIDGET_APP);
}

// Устанавливает главный виджет метода по айди
void MainWindow::setMainWidget(int idWidget) {

    // Очищаем старые сигнально слотовые соединения
    if(currentWidget) {
        disconnect(currentWidget, SIGNAL(changeCurrentWidget(int,bool)), this, SLOT(setMainWidgetSlot(int,bool)));
    }

    // Получаем виджет по айди
    currentWidget = getWidgetById(idWidget);
    currentWidget->setVisible(true);

    // Делаем виджет центральным для этого окна
    setCentralWidget(currentWidget);

    connect(currentWidget, SIGNAL(changeCurrentWidget(int,bool)), SLOT(setMainWidgetSlot(int,bool)));
}

// Возвращает виджет по айди
// Если виджет не создан - под него выделяется память
AbstractWidget* MainWindow::getWidgetById(int idWidget) {

    // Проверка на наличие виджета в таблице
    if(widgetsHash.contains(idWidget) && widgetsHash[idWidget] != nullptr) {
        return widgetsHash[idWidget];
    }

    AbstractWidget* returnWidget = nullptr;

    switch(idWidget) {

        case WidgetIdContainer::MAIN_WIDGET_APP:
            returnWidget = new MainWidget(this);
            break;
    }

    return returnWidget;
}

// Слот коллбэк - устанавливаем главный виджет
// Флаг deleteFlag:
// true - удалить текущий виджет
// false - оставить текущий виджет в памяти
void MainWindow::setMainWidgetSlot(int idWidget, bool deleteFlag) {

    AbstractWidget* buffer = currentWidget;

    setMainWidget(idWidget);

    if(deleteFlag && buffer != nullptr) {
        delete buffer;
    }
}

MainWindow::~MainWindow() {}

