#include "mainwindow.h"
#include <QApplication>
#include <WidgetIdContainer.h>

const int WidgetIdContainer::MAIN_WIDGET_APP;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
