#ifndef ABSTRACTWIDGET_H
#define ABSTRACTWIDGET_H

#include "WidgetIdContainer.h"
#include <QWidget>

class AbstractWidget : public QWidget
{
    Q_OBJECT
public:
    explicit AbstractWidget(QWidget *parent = 0);

signals:
    void changeCurrentWidget(int widgetId, bool deleteSelfFlag);
};

#endif // ABSTRACTWIDGET_H
