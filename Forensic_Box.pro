#-------------------------------------------------
#
# Project created by QtCreator 2014-11-12T02:26:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Forensic_Box
TEMPLATE = app


SOURCES += main.cpp \
    MainWindow.cpp \
    AbstractWidget.cpp \
    MainWidget.cpp \
    UsbFinder.cpp \
    UsbFlash.cpp

HEADERS  += \
    MainWindow.h \
    WidgetIdContainer.h \
    AbstractWidget.h \
    MainWidget.h \
    UsbFinder.h \
    UsbFlash.h


FORMS    += \
    MainWidget.ui

QMAKE_MAC_SDK = macosx10.9
CONFIG += c++11
